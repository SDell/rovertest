﻿using PlumGuide.RoverNavigation;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace PlumGuide.RoverNavigationTests.Unit
{
    public class RoverMapTests
    {
        [Fact]
        public void HorizontalWrappingingWorksGoingWest()
        {
            //Arrange
            var sut = new RoverMap(100, 100);
            var initialPosition = new RoverPoint(0, 0);
            var expected = new RoverPoint(100, 0);

            //Act
            var actual = sut.WestPoint(initialPosition);

            //Asset
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void HorizontalWrappingingWorksGoingEast()
        {
            //Arrange
            var sut = new RoverMap(100, 100);
            var initialPosition = new RoverPoint(100, 0);
            var expected = new RoverPoint(0, 0);

            //Act
            var actual = sut.EastPoint(initialPosition);

            //Asset
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void VerticalWrappingingWorksGoingNorth()
        {
            //Arrange
            var sut = new RoverMap(100, 100);
            var initialPosition = new RoverPoint(100, 100);
            var expected = new RoverPoint(50, 100, true);

            //Act
            var actual = sut.NorthPoint(initialPosition);

            //Asset
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void VerticalWrappingingWorksGoingNorth2()
        {
            //Arrange
            var sut = new RoverMap(100, 100);
            var initialPosition = new RoverPoint(30, 100);
            var expected = new RoverPoint(80, 100, true);

            //Act
            var actual = sut.NorthPoint(initialPosition);

            //Asset
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void VerticalWrappingingWorksGoingSouth()
        {
            //Arrange
            var sut = new RoverMap(100, 100);
            var initialPosition = new RoverPoint(100, 0);
            var expected = new RoverPoint(50, 0, true);

            //Act
            var actual = sut.SouthPoint(initialPosition);

            //Asset
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void VerticalWrappingingWorksGoingSouthUnevenMap()
        {
            //Arrange
            var sut = new RoverMap(100, 50);
            var initialPosition = new RoverPoint(100, 0);
            var expected = new RoverPoint(50, 0, true);

            //Act
            var actual = sut.SouthPoint(initialPosition);

            //Asset
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void VerticalWrappingingWorksGoingNorthUnevenMap()
        {
            //Arrange
            var sut = new RoverMap(100, 50);
            var initialPosition = new RoverPoint(100, 50);
            var expected = new RoverPoint(50, 50, true);

            //Act
            var actual = sut.NorthPoint(initialPosition);

            //Asset
            Assert.Equal(expected, actual);
        }
    }
}
