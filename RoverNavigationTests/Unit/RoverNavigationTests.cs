﻿using PlumGuide.RoverNavigation;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace PlumGuide.RoverNavigationTests.Unit
{
    public class RoverNavigationTests
    {
        [Fact]
        public void NavigateAcceptsLetterF()
        {
            //Arrange
            var sut = new RoverNavigation.RoverNavigation(new RoverMap(100, 100), new RoverPoint(0, 0), RoverDirection.North);
            var command = "F";
            var expected = new RoverPoint(0, 1);

            //Act
            var actual = sut.Navigate(command);

            //Asset
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void NavigateAcceptsLetterB()
        {
            //Arrange
            var sut = new RoverNavigation.RoverNavigation(new RoverMap(100, 100), new RoverPoint(0, 1), RoverDirection.North);
            var command = "B";
            var expected = new RoverPoint(0, 0);

            //Act
            var actual = sut.Navigate(command);

            //Asset
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void NavigateAcceptsLetterFF()
        {
            //Arrange
            var sut = new RoverNavigation.RoverNavigation(new RoverMap(100, 100), new RoverPoint(0, 0), RoverDirection.North);
            var command = "FF";
            var expected = new RoverPoint(0, 2);

            //Act
            var actual = sut.Navigate(command);

            //Asset
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void NavigateAcceptsLetterFFB()
        {
            //Arrange
            var sut = new RoverNavigation.RoverNavigation(new RoverMap(100, 100), new RoverPoint(0, 0), RoverDirection.North);
            var command = "FFB";
            var expected = new RoverPoint(0, 1);

            //Act
            var actual = sut.Navigate(command);

            //Asset
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void NavigateAcceptsLetterFRF()
        {
            //Arrange
            var sut = new RoverNavigation.RoverNavigation(new RoverMap(100, 100), new RoverPoint(0, 0), RoverDirection.North);
            var command = "FRRRRRF";
            var expected = new RoverPoint(1, 1);

            //Act
            var actual = sut.Navigate(command);

            //Asset
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void NavigateAcceptsLetterFLF()
        {
            //Arrange
            var sut = new RoverNavigation.RoverNavigation(new RoverMap(100, 100), new RoverPoint(50, 50), RoverDirection.North);
            var command = "FLLLLLF";
            var expected = new RoverPoint(49, 51);

            //Act
            var actual = sut.Navigate(command);

            //Asset
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void NavigateAcceptsLetterBLB()
        {
            //Arrange
            var sut = new RoverNavigation.RoverNavigation(new RoverMap(100, 100), new RoverPoint(50, 50), RoverDirection.North);
            var command = "BLLLLLB";
            var expected = new RoverPoint(51, 49);

            //Act
            var actual = sut.Navigate(command);

            //Asset
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void NavigateWithObstacleStopsMovementNorth()
        {
            //Arrange
            var obstacles = new List<RoverPoint>
            {
                new RoverPoint(10,11)
            };
            var map = new RoverMap(100, 100, obstacles.ToArray());
            var sut = new RoverNavigation.RoverNavigation(map, new RoverPoint(10, 10), RoverDirection.North);
            var command = "F";
            var expected = new RoverPoint(10, 10);

            //Act
            var actual = sut.Navigate(command);

            //Asset
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void NavigateWithObstacleStopsMovementEast()
        {
            //Arrange
            var obstacles = new List<RoverPoint>
            {
                new RoverPoint(11,10)
            };
            var map = new RoverMap(100, 100, obstacles.ToArray());
            var sut = new RoverNavigation.RoverNavigation(map, new RoverPoint(10, 10), RoverDirection.North);
            var command = "RF";
            var expected = new RoverPoint(10, 10);

            //Act
            var actual = sut.Navigate(command);

            //Asset
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void NavigateWithObstacleStopsMovementSouth()
        {
            //Arrange
            var obstacles = new List<RoverPoint>
            {
                new RoverPoint(10, 9)
            };
            var map = new RoverMap(100, 100, obstacles.ToArray());
            var sut = new RoverNavigation.RoverNavigation(map, new RoverPoint(10, 10), RoverDirection.North);
            var command = "B";
            var expected = new RoverPoint(10, 10);

            //Act
            var actual = sut.Navigate(command);

            //Asset
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void NavigateWithObstacleStopsMovementWest()
        {
            //Arrange
            var obstacles = new List<RoverPoint>
            {
                new RoverPoint(9,10)
            };
            var map = new RoverMap(100, 100, obstacles.ToArray());
            var sut = new RoverNavigation.RoverNavigation(map, new RoverPoint(10, 10), RoverDirection.North);
            var command = "LF";
            var expected = new RoverPoint(10, 10);

            //Act
            var actual = sut.Navigate(command);

            //Asset
            Assert.Equal(expected, actual);
        }
    }
}
