﻿using PlumGuide.RoverNavigation;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace PlumGuide.RoverNavigationTests.Acceptance
{
    public class NavigationTests
    {
        [Fact]
        public void GivenExample()
        {
            //Arrange
            var sut = new RoverNavigation.RoverNavigation(new RoverMap(100,100), new RoverPoint(0, 0), RoverDirection.North);
            var command = "FFRFF";
            var expected = new RoverPoint(2, 2);

            //Act
            var actual = sut.Navigate(command);

            //Asset
            Assert.Equal(expected, actual);
            Assert.Equal(RoverDirection.East, sut.RoverDirection);
        }

        [Fact]
        public void HorizontalWrappingShouldTurnRoverSouthBound()
        {
            //Arrange
            var sut = new RoverNavigation.RoverNavigation(new RoverMap(100, 100), new RoverPoint(0, 0), RoverDirection.North);
            var command = "BB";
            var expected = new RoverPoint(50, 1);

            //Act
            var actual = sut.Navigate(command);

            //Asset
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void HorizontalWrappingShouldTurnRoverNorthBound()
        {
            //Arrange
            var sut = new RoverNavigation.RoverNavigation(new RoverMap(100, 100), new RoverPoint(0, 100), RoverDirection.North);
            var command = "FF";
            var expected = new RoverPoint(50, 99);

            //Act
            var actual = sut.Navigate(command);

            //Asset
            Assert.Equal(expected, actual);
        }
    }
}
