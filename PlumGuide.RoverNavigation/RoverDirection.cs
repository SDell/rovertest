﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlumGuide.RoverNavigation
{
    public enum RoverDirection
    {
        North = 0,
        East,
        South,
        West
    }
}
