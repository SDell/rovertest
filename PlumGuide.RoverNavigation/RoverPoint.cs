﻿using System;

namespace PlumGuide.RoverNavigation
{
    public struct RoverPoint
    {
        private int X { get; }
        private int Y { get; }

        public bool WasHorizontalJump { get; }
        public bool IsOnObstacle { get; }

        public RoverPoint(int x, int y, bool horizontalJump = false, bool obstacle = false)
        {
            this.X = x;
            this.Y = y;
            this.WasHorizontalJump = horizontalJump;
            this.IsOnObstacle = obstacle;
        }

        public RoverPoint Up()
        {
            return new RoverPoint(this.X, this.Y + 1);
        }

        public RoverPoint Down()
        {
            return new RoverPoint(this.X, this.Y - 1);
        }

        public RoverPoint Right()
        {
            return new RoverPoint(this.X + 1, this.Y);
        }

        public RoverPoint Left()
        {
            return new RoverPoint(this.X - 1, this.Y);
        }

        public RoverPoint GetHorizontalPoint(int specificX)
        {
            return new RoverPoint(specificX, this.Y);
        }

        public bool isX(int x)
        {
            return this.X == x;
        }

        public bool isY(int y)
        {
            return this.Y == y;
        }

        public RoverPoint GetJumpedHorizontalPoint(int horizontalJump)
        {
            var newX = this.X - horizontalJump < 0 ? this.X + horizontalJump : this.X - horizontalJump;
            return new RoverPoint(newX, this.Y, true);
        }

        public RoverPoint CloneOnObstacle()
        {
            return new RoverPoint(this.X, this.Y, this.WasHorizontalJump, true);
        }
    }
}