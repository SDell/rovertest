﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PlumGuide.RoverNavigation
{
    public class RoverMap
    {
        protected virtual int X { get; set; }
        protected virtual int Y { get; set; }
        protected virtual IEnumerable<RoverPoint> Obstacles { get; }

        public RoverMap(int x, int y, params RoverPoint[] obstacles)
        {
            this.X = x;
            this.Y = y;
            this.Obstacles = obstacles ?? Enumerable.Empty<RoverPoint>();
        }

        public virtual RoverPoint NorthPoint(RoverPoint roverPoint)
        {
            return this.VerticalPoint(roverPoint, roverPoint.isY(this.Y), roverPoint.Up());
        }

        public virtual RoverPoint SouthPoint(RoverPoint roverPoint)
        {
            return this.VerticalPoint(roverPoint, roverPoint.isY(0), roverPoint.Down());
        }

        public virtual RoverPoint EastPoint(RoverPoint roverPoint)
        {
            return this.HorizontalPoint(roverPoint, roverPoint.isX(this.X), 0, roverPoint.Right());
        }

        public virtual RoverPoint WestPoint(RoverPoint roverPoint)
        {
            return this.HorizontalPoint(roverPoint, roverPoint.isX(0), this.X, roverPoint.Left());
        }

        protected virtual RoverPoint VerticalPoint(RoverPoint currentPoint, bool isOnEdge, RoverPoint nextPossiblePoint)
        {
            if (isOnEdge)
            {
                var horizontalJump = this.X / 2;
                return currentPoint.GetJumpedHorizontalPoint(horizontalJump);
            }
            var nextPoint = this.MarkIfOnObstacle(nextPossiblePoint);
            return nextPoint;
        }

        protected virtual RoverPoint HorizontalPoint(RoverPoint currentPoint, bool isOnEdge, int nextX, RoverPoint nextPossiblePoint)
        {
            if (isOnEdge)
            {
                return currentPoint.GetHorizontalPoint(nextX);
            }
            var nextPoint = this.MarkIfOnObstacle(nextPossiblePoint);
            return nextPoint;
        }

        protected virtual RoverPoint MarkIfOnObstacle(RoverPoint point)
        {
            if (this.Obstacles.Contains(point))
            {
                return point.CloneOnObstacle();
            }
            return point;
        }
    }
}