﻿using System;
using System.Collections.Generic;

namespace PlumGuide.RoverNavigation
{
    public class RoverNavigation
    {
        public virtual RoverDirection RoverDirection { get; protected set; }
        protected virtual RoverMap RoverMap { get; set; }
        protected virtual RoverPoint RoverPoint { get; set; }

        protected virtual Dictionary<char, Func<bool>> Commands => new Dictionary<char, Func<bool>>
        {
            { 'F', MoveForward },
            { 'B', MoveBackward },
            { 'R', RotateRight },
            { 'L', RotateLeft }
        };

        protected virtual Dictionary<RoverDirection, Func<RoverPoint, RoverPoint>> ForwardMoves => new Dictionary<RoverDirection, Func<RoverPoint, RoverPoint>>
        {
            { RoverDirection.North, this.RoverMap.NorthPoint},
            { RoverDirection.West, this.RoverMap.WestPoint},
            { RoverDirection.South, this.RoverMap.SouthPoint},
            { RoverDirection.East, this.RoverMap.EastPoint}
        };

        protected virtual Dictionary<RoverDirection, Func<RoverPoint, RoverPoint>> BackwardMoves => new Dictionary<RoverDirection, Func<RoverPoint, RoverPoint>>
        {
            { RoverDirection.North, this.RoverMap.SouthPoint},
            { RoverDirection.West, this.RoverMap.EastPoint},
            { RoverDirection.South, this.RoverMap.NorthPoint},
            { RoverDirection.East, this.RoverMap.WestPoint}
        };

        public RoverNavigation(RoverMap roverMap, RoverPoint roverPoint, RoverDirection direction)
        {
            this.RoverMap = roverMap;
            this.RoverPoint = roverPoint;
            this.RoverDirection = direction;
        }

        public RoverPoint Navigate(string command)
        {
            foreach (var letter in command)
            {
                if (!Commands.ContainsKey(letter)) break;
                var roverMoved = Commands[letter]();
                if (!roverMoved) break;
            }
            return this.RoverPoint;
        }

        protected virtual bool MoveForward()
        {
            return Move(this.ForwardMoves);
        }

        protected virtual bool MoveBackward()
        {
            return Move(this.BackwardMoves);
        }

        protected virtual bool RotateRight()
        {
            return Rotate(RoverDirection.West, RoverDirection.North, 1);
        }

        protected virtual bool RotateLeft()
        {
            return Rotate(RoverDirection.North, RoverDirection.West, -1);
        }

        protected virtual bool Move(Dictionary<RoverDirection, Func<RoverPoint, RoverPoint>> moves)
        {
            if (!moves.ContainsKey(this.RoverDirection))
                return false;
            var nextPoint = moves[this.RoverDirection](this.RoverPoint);
            if (nextPoint.IsOnObstacle)
            {
                return false;
            }
            this.RoverPoint = nextPoint;
            this.AdaptForVerticalWrapping();
            return true;
        }

        protected virtual bool Rotate(RoverDirection condition, RoverDirection resetValue, int addtion)
        {
            this.RoverDirection = this.RoverDirection == condition ? resetValue : this.RoverDirection + addtion;
            return true;
        }

        protected virtual void AdaptForVerticalWrapping()
        {
            if (this.RoverPoint.WasHorizontalJump)
            {
                RotateLeft();
                RotateLeft();
            }
        }
    }
}